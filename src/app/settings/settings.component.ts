import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  viewForm: FormGroup;
  noParkingForm: FormGroup;
  junctions: any = [];
  cams: any = [];
  selectedJunction: any;
  selectedCam: any;
  no_parking_zones: FormArray;
  showViewForm:boolean=false;
  showNoParkingForm:boolean=false;
  isViewFormEdit:boolean=false;
  isNoParkingFormEdit:boolean=false;
  noPZIndex=-1;
  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.getJunctiuons();
  }


  getJunctiuons() {
    this.junctions = [
      { name: 'Junction 1', value: 'j1' },
      { name: 'Junction 2', value: 'j2' },
      { name: 'Junction 3', value: 'j3' },
      { name: 'Junction 4', value: 'j4' },
      { name: 'Junction 5', value: 'j5' },
      { name: 'Junction 6', value: 'j6' },
      { name: 'Junction 7', value: 'j7' },
      { name: 'Junction 8', value: 'j8' },
      { name: 'Junction 9', value: 'j9' }
    ]
  }

  onJunctionSelect(jun) {
    this.selectedJunction = jun;
    this.selectedCam = null;
    this.cams = [
      { name: 'Camera 1', value: 'c1' },
      { name: 'Camera 2', value: 'c2' },
      { name: 'Camera 3', value: 'c3' },
      { name: 'Camera 4', value: 'c4' },
      { name: 'Camera 5', value: 'c5' },
      { name: 'Camera 6', value: 'c6' },
      { name: 'Camera 7', value: 'c7' },
      { name: 'Camera 8', value: 'c8' },
      { name: 'Camera 9', value: 'c9' }
    ]
  }

  onCamSelect(cam) {
    this.selectedCam = cam;
    this.initiateviewForm();
    this.initiatenoParkingForm();
    this.viewForm.disable();
    // this.noParkingForm.disable();
    this.showNoParkingForm = true;
    this.showViewForm = true;
  }

  initiateviewForm() {
    this.viewForm = this.fb.group({
      xtl: ['', Validators.required],
      ytl: ['', Validators.required],
      xtr: ['', Validators.required],
      ytr: ['', Validators.required],
      xbl: ['', Validators.required],
      ybl: ['', Validators.required],
      xbr: ['', Validators.required],
      ybr: ['', Validators.required]
    })
  }

  initiatenoParkingForm() {
    this.noParkingForm = this.fb.group({
      no_parking_zone: this.fb.array([this.createNewFields()])
    })
  }

  get no_parking_zone() {
    return this.noParkingForm.get('no_parking_zone') as FormArray;
  }

  createNewFields(): FormGroup {
    return this.fb.group({
      xtl: ['', Validators.required],
      ytl: ['', Validators.required],
      xtr: ['', Validators.required],
      ytr: ['', Validators.required],
      xbl: ['', Validators.required],
      ybl: ['', Validators.required],
      xbr: ['', Validators.required],
      ybr: ['', Validators.required]
    });
  }

  addNoParkingZone(i){
    this.noPZIndex=i+1;
    this.isNoParkingFormEdit=true;
    this.no_parking_zones = this.no_parking_zone;
    this.no_parking_zones.push(this.createNewFields());
  }

  removeNoParkingZone(i){
    this.noPZIndex=-1;
    this.isNoParkingFormEdit=false;
    this.no_parking_zone.removeAt(i);
    this.no_parking_zones = this.no_parking_zone;
  }

  submitViewForm(fd) {
    console.log(fd);
    this.viewForm.disable();
    this.isViewFormEdit=false;
  }

  submitNoParkingForm(fd) {
    console.log(fd);
    this.noPZIndex=-1;
    // this.noParkingForm.disable();
    this.isNoParkingFormEdit=false;
  }

}
