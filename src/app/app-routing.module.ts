import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { JunctionsComponent } from './junctions/junctions.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { VideostreamComponent } from './videostream/videostream.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  { path: "", redirectTo:  "login", pathMatch: "full" },
  {path:"login",component:LoginpageComponent},
  {path:"dashboard",component:DashboardComponent,canActivate:[AuthGuard]},
  {path:"junctions",component:JunctionsComponent,canActivate:[AuthGuard]},
  {path:"watchlist",component:WatchlistComponent,canActivate:[AuthGuard]},
  {path:"camstreaming",component:VideostreamComponent,canActivate:[AuthGuard]},
  {path:"settings",component:SettingsComponent,canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
