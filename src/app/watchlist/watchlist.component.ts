import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { trackingData } from '../datasets';
@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  inputType: string;
  cols: any = [];
  historyData: any = [];
  inputForm: FormGroup;
  vehicle_nums: FormArray;
  activeTabs:any=0;
  imagecard: boolean = false;
  numberPlateCard : boolean = false;
  selectedImages:any;
  selectedRegNums:any;
  constructor(private fb: FormBuilder, private router: Router) { }
  ngOnInit() {
    this.initiateForm();
  }
  onIPTypeSelect(t) {
    this.numberPlateCard = false;
    this.imagecard = false;
    this.inputType = t;
    this.activeTabs = 0;
    this.vehicle_nums=null;
    this.selectedImages=[];
    this.selectedRegNums=[];
    this.initiateForm();
  }
  initiateForm() {
    this.inputForm = this.fb.group({
      vehicle_num: this.fb.array([this.createnewField()])
    })
  }
  get vehicle_num() {
    return this.inputForm.get('vehicle_num') as FormArray;
  }

  createnewField() {
    return this.fb.group({
      reg_num: ['', Validators.required]
    })
  }

  addvehclenum() {
    if (this.inputForm.invalid) {
      return;
    }
    this.vehicle_nums = this.vehicle_num;
    this.vehicle_nums.push(this.createnewField());
  }

  removevehclenum(i) {
    this.vehicle_num.removeAt(i);
    this.vehicle_nums = this.vehicle_num;
  }

  uploadImages(files) {
    this.selectedImages = [];
    for (let i = 0; i < files.length; i++) {
      let reader = new FileReader();
      reader.readAsDataURL(files[i]);
      reader.onload = (_event) => {
        this.selectedImages.push(reader.result);
      }
    }
    this.getPersonsTrackingData();
  }

  getPersonsTrackingData(){
    this.historyData = trackingData.faces;
    this.cols = [
      { field: 'junction', header: 'Junction' },
      { field: 'time', header: 'Date & Time ' },
      { field: 'image', header: 'Image' },
      { field: 'clothes_color', header: 'Clothes Color' }
    ];
    this.imagecard = true;
  }


  onSubmit(fd) {
    this.selectedRegNums=[];
    fd.vehicle_num.filter(ele=>{
      this.selectedRegNums.push(ele.reg_num);
    })
    this.activeTabs = [];
    this.historyData = trackingData.vehicles;
      this.numberPlateCard = true;
      this.cols = [
        { field: 'junction', header: 'Junction' },
        { field: 'time', header: 'Date & Time ' },
        { field: 'vehicle_image', header: 'Vehicle Image' },
        { field: 'rider_image', header: 'Rider Image' }
      ];
  }

scrollToTable(el){
  document.getElementById(el).scrollIntoView();
}

}
