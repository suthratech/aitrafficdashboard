import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JunctionsData } from '../datasets';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
import { PoliceService } from '../police.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-junctions',
  templateUrl: './junctions.component.html',
  styleUrls: ['./junctions.component.css']
})
export class JunctionsComponent implements OnInit {
  innerWidth: any;
  cols: any = [];
  tableData: any = [];
  junctions: any = [];
  vehicleTypes: any = [];
  colors: any = [];
  vehicleMakes: any = [];
  helmetStatus: any = [];
  tripleRideStatus: any = [];
  drTypes: any = [];
  filtersForm: FormGroup;
  dynamicCards;
  marqueeImages: any;
  timeout = 1000;
  constructor(private fb: FormBuilder, private router: Router, private service:PoliceService) {

  }
  ngOnInit() {
    this.getAllData();
    this.getMarqueeImages();
    this.initiateCols();
    this.initiateForm();
    this.initiateDropdownsData();
    // this.generateDynamicCardsData();
    this.innerWidth = window.innerWidth;
  }
  routing() {
    this.router.navigateByUrl('/camstreaming');
  }

  getAllData(){
    this.service.getVehiclesData().subscribe(res=>{
      console.log(res);
      let resp:any =res;
      this.tableData = resp.response.data;
      this.generateDynamicCardsData(resp.response);
    })
  }
  generateDynamicCardsData(data) {
    this.dynamicCards = [
      { vehicle_name: 'Cars', count: data.cars, icon: 'drive_eta', cssClass: 'first_circle' },
      { vehicle_name: 'Bus', count: data.bus, icon: 'directions_bus', cssClass: 'second_circle' },
      { vehicle_name: 'Motor Bikes', count: data.motor_bikes, icon: 'motorcycle', cssClass: 'third_circle' },
      { vehicle_name: 'Emergency', count: data.emergency, icon: 'airport_shuttle', cssClass: 'fourth_circle' },
      { vehicle_name: 'Auto', count: data.auto, icon: 'supervisor_account', cssClass: 'fourth_circle' },
      { vehicle_name: 'Riders', count: data.riders, icon: 'directions_bike', cssClass: 'third_circle' },
      { vehicle_name: 'Trucks', count: data.trucks, icon: 'local_shipping', cssClass: 'second_circle' },
      { vehicle_name: 'Pedestrians', count: data.pedestrians, icon: 'directions_walk', cssClass: 'first_circle' },
    ]
  }
  getMarqueeImages() {
    this.marqueeImages = [
      'traffic.jpg', 'traffic1.jpg', 'traffic3.jpg', 'traffic2.jpg', 'biketracking.jpg', 'bikeriderimg.jpg', 'ambulanceplate.jpg', 'ambulancetraffic.jpg',
      'Ambulance.jpg', 'apsrtcbusplate.jpg', 'apsrtcbustraffic.jpg', 'apsrtcbus.jpg', 'autoplate.jpeg', 'autotraffic.jpg', 'auto.jpg', 'traffic.jpg', 'traffic1.jpg', 'traffic3.jpg', 'traffic2.jpg', 'biketracking.jpg', 'bikeriderimg.jpg', 'ambulanceplate.jpg', 'ambulancetraffic.jpg',
      'Ambulance.jpg', 'apsrtcbusplate.jpg', 'apsrtcbustraffic.jpg', 'apsrtcbus.jpg', 'autoplate.jpeg', 'autotraffic.jpg', 'auto.jpg', 'traffic.jpg', 'traffic1.jpg', 'traffic3.jpg', 'traffic2.jpg', 'biketracking.jpg', 'bikeriderimg.jpg', 'ambulanceplate.jpg', 'ambulancetraffic.jpg',
      'Ambulance.jpg', 'apsrtcbusplate.jpg', 'apsrtcbustraffic.jpg', 'apsrtcbus.jpg', 'autoplate.jpeg', 'autotraffic.jpg', 'auto.jpg', 'traffic.jpg', 'traffic1.jpg', 'traffic3.jpg', 'traffic2.jpg', 'biketracking.jpg', 'bikeriderimg.jpg', 'ambulanceplate.jpg', 'ambulancetraffic.jpg',
      'Ambulance.jpg', 'apsrtcbusplate.jpg', 'apsrtcbustraffic.jpg', 'apsrtcbus.jpg', 'autoplate.jpeg', 'autotraffic.jpg', 'auto.jpg'
    ];
  }
  initiateDropdownsData() {
    this.junctions = [
      { name: 'Junction 1', value: 'j1' },
      { name: 'Junction 2', value: 'j2' },
      { name: 'Junction 3', value: 'j3' },
      { name: 'Junction 4', value: 'j4' },
      { name: 'Junction 5', value: 'j5' },
      { name: 'Junction 6', value: 'j6' },
      { name: 'Junction 7', value: 'j7' },
      { name: 'Junction 8', value: 'j8' },
      { name: 'Junction 9', value: 'j9' }
    ]
    this.drTypes = [
      { name: 'MTD', value: 'mtd' },
      { name: 'YTD', value: 'ytd' }
    ];
    this.vehicleMakes = [
      { name: 'Audi', value: 'audi' },
      { name: 'BMW', value: 'bmw' },
      { name: 'Chevrolet', value: 'chevrolet' },
      { name: 'Dodge', value: 'dodge' },
      { name: 'Fiat', value: 'fiat' },
      { name: 'Ford', value: 'ford' },
      { name: 'Honda', value: 'honda' },
      { name: 'Hyundai', value: 'hyundai' },
      { name: 'Jeep', value: 'jeep' },
      { name: 'Kia', value: 'kia' },
      { name: 'Land Rover', value: 'landrover' },
      { name: 'Mercedes', value: 'mercedes' },
      { name: 'Mistubishi', value: 'mistubishi' },
      { name: 'Nissan', value: 'nissan' },
      { name: 'Renault', value: 'renault' },
      { name: 'Skoda', value: 'skoda' },
      { name: 'Suzuki', value: 'suzuki' },
      { name: 'Tesla', value: 'tesla' },
      { name: 'Toyota', value: 'toyota' },
      { name: 'Volkswagen', value: 'volkswagen' },
      { name: 'Volvo', value: 'volvo' }
    ];
    this.vehicleTypes = [
      { name: 'Ambulance', value: 'ambulance' },
      { name: 'Auto', value: 'auto' },
      { name: 'Bus', value: 'bus' },
      { name: 'Car', value: 'car' },
      { name: 'Fire Truck', value: 'fire truck' },
      { name: 'Motor Bike', value: 'motor bike' },
      { name: 'Truck', value: 'truck' }
    ];
    this.colors = [
      { name: 'Black', value: 'black' },
      { name: 'Blue', value: 'blue' },
      { name: 'Gray', value: 'gray' },
      { name: 'Green', value: 'green' },
      { name: 'Red', value: 'red' },
      { name: 'Violet', value: 'violet' },
      { name: 'White', value: 'white' },
      { name: 'Yellow', value: 'yellow' }
    ];
    this.helmetStatus = [
      { name: 'Have Helmet', value: 'yes' },
      { name: 'No Helmet', value: 'no' }
    ]
  }

  initiateForm() {
    this.filtersForm = this.fb.group({
      junction: [''],
      fromDate: [new Date(new Date().setHours(0, 0, 0, 0))],
      toDate: [new Date(new Date().setHours(0, 0, 0, 0))],
      vehicle_type: [''],
      color: [''],
      vehicle_make: [''],
      helmet: [''],
      tripleRide: ['']
    });
  }

  initiateCols() {
    this.cols = [
      { field: 'time_stamp', header: 'Date & Time' },
      { field: 'license_plate_number', header: 'License Plate Number' },
      { field: 'license_plate_image_file', header: 'Number Plate' },
      { field: 'image_file', header: 'Vehicle Image' },
      { field: 'vehicle_type', header: 'Vehicle Type' },
      { field: 'emergency_type', header: 'Emergency Type' },
      { field: 'color', header: 'Vehicle Color' },
      { field: 'make_model', header: 'Vehicle Make' }
    ];
  }

 async generateDataforReport(action) {
    let arrays: any = []; 
   await this.tableData.forEach(ele => {
     let obj = {
      time_stamp:ele.time_stamp,
      license_plate_number:ele.license_plate_number,
      license_plate_image_file:ele.license_plate_image_file,
      image_file:ele.image_file,
      vehicle_type:ele.vehicle_type,
      emergency_type:ele.emergency_type,
      color:ele.color,
      make_model:ele.make_model
     }
      let arr = this.formArrayData(obj);
      arrays.push(arr);
    });
    this.generateReport(arrays, action);
  }
  async getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();

    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    });
  }
    formArrayData(ele) {
    let arr: any = new Array(8);
    let keys = Object.keys(ele);
    let values = Object.values(ele);
    for (let i = 0; i < keys.length; i++) {

      if (keys[i] != 'license_plate_image_file' && keys[i] != 'image_file'  && keys[i] != 'time_stamp' ) {
        let d:any = values[i]?values[i]:'N/A';
        arr[i] = { text: d.toUpperCase(), fontSize: 8 };
      }
      if(keys[i] == 'time_stamp'){
        let d:any = values[i] ;
        arr[i] = { text: formatDate(new Date(d),'MMM d, y, hh:mm:ss a', 'en-US', '+0530'), fontSize: 8 };
      }
      if (keys[i] == 'license_plate_image_file' || keys[i] == 'image_file') {
        // let img = await this.getBase64ImageFromUrl('../../assets/' + values[i]);
        // arr[i] = { image: img, width: 70 };
        this.getBase64ImageFromUrl( values[i]?'../../assets/' + values[i]:'../../assets/no_image.png').then(result => { arr[i] = { image: result, fit: [70, 70] } });
      }
    }
    return arr;
  }

  generateReport(trows, action) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    let data: any = [];
    data.push([
      { text: 'Date & Time', alignment: 'center', fontSize: 10 },
      { text: 'License Plate Number', alignment: 'center', fontSize: 10 },
      { text: 'Number Plate', alignment: 'center', fontSize: 10 },
      { text: 'Vehicle Image', alignment: 'center', fontSize: 10 },
      { text: 'Vehicle Type', alignment: 'center', fontSize: 10 },
      { text: 'Emergency Type', alignment: 'center', fontSize: 10 },
      { text: 'Vehicle Color', alignment: 'center', fontSize: 10 },
      { text: 'Vehicle Make', alignment: 'center', fontSize: 10 }
    ]);
    data = [...data, ...trows];
    var dd = {
      content: [
        { text: 'Nellore Police', style: 'header', alignment: 'center' },
        { text: 'Junctions Wise Report', style: 'subheader', alignment: 'center' },
        {
          columns: [
            { text: "Date: Feb 04,2020", alignment: "left", fontSize: 8 },
            { text: "Junction: Junction 1", alignment: "right", fontSize: 8 }
          ]
        },
        {
          style: 'tableExample',
          widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
          table: {
            headerRows: 1,
            body: data,
            dontBreakRows: true
          }
        }
      ],
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [0, 0, 0, 5]
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15],
          alignment: 'center'
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      pageOrientation: 'landscape',
      defaultStyle: {
        alignment: 'justify'
      }
    }
    setTimeout(() => {
    action == 'open' ? pdfMake.createPdf(dd).open() : pdfMake.createPdf(dd).download();
    }, 3000);


  }

}
