export const trackingData = {
  faces:[
    {
      junction: 'Junction 1',
      time: 'Jan-02-2020, 12:15 PM ',
      image: 'traffic.jpg',
      clothes_color: 'White'
    },
    {
      junction: 'Junction 2',
      time: 'Jan-02-2020, 12:15 PM ',
      image: 'traffic1.jpg',
      clothes_color: 'White'
    },
    {
      junction: 'Junction 3',
      time: 'Jan-02-2020, 12:15 PM ',
      image: 'traffic3.jpg',
      clothes_color: 'White'
    },
    {
      junction: 'Junction 4',
      time: 'Jan-02-2020, 12:15 PM ',
      image: 'traffic2.jpg',
      clothes_color: 'White'
    }
  ],
  vehicles:[
    {
      junction: 'Junction 1',
      time: 'Jan-01-2020, 06:15 AM ',
      vehicle_image: 'biketracking.jpg',
      rider_image: 'bikeriderimg.jpg'
    },
    {
      junction: 'Junction 2',
      time: 'Jan-02-2020, 1:15 PM ',
      vehicle_image: 'biketracking.jpg',
      rider_image: 'bikeriderimg.jpg'
    },
    {
      junction: 'Junction 3',
      time: 'Jan-03-2020, 12:15 AM ',
      vehicle_image: 'biketracking.jpg',
      rider_image: 'bikeriderimg.jpg'
    },
    {
      junction: 'Junction 4',
      time: 'Jan-04-2020, 12:15 PM ',
      vehicle_image: 'biketracking.jpg',
      rider_image: 'bikeriderimg.jpg'
    },
    {
      junction: 'Junction 5',
      time: 'Jan-06-2020, 2:15 AM ',
      vehicle_image: 'biketracking.jpg',
      rider_image: 'bikeriderimg.jpg'
    }, 
  ]
}


export const JunctionsData = {
    jdata: [{
        reg_no: 'AP 9 BD 3459',
        num_plate: 'ambulanceplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'ambulancetraffic.jpg',
        vehicle_type: 'Ambulance',
        color: 'White',
        vehicle_make: 'Force',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'Ambulance.jpg'
    },
    {
        reg_no: 'AP 31 DD 085',
        num_plate: 'apsrtcbusplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'apsrtcbustraffic.jpg',
        vehicle_type: 'Bus',
        color: 'Yellow',
        vehicle_make: 'Mahendra',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'apsrtcbus.jpg'
    },
    {
        reg_no: 'AP 11 M 3804',
        num_plate: 'autoplate.jpeg',
        num_plate_color: 'White',
        vehicle_img: 'autotraffic.jpg',
        vehicle_type: 'Auto',
        color: 'Yellow',
        vehicle_make: 'TATA Motors',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'auto.jpg'
    },
    {
        reg_no: 'AP 11 BN 4393',
        num_plate: 'bikeplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'biketraffic.jpg',
        vehicle_type: 'Bike',
        color: 'Black',
        vehicle_make: 'Bajaj',
        helmet: 'No',
        triples: 'No',
        full_image: 'bike.jpg'
    },
    {
        reg_no: 'AP 31 CG 3600',
        num_plate: 'busplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'bustraffic.jpg',
        vehicle_type: 'BUS',
        color: 'White',
        vehicle_make: 'Force',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'bus.jpg'
    },
    {
        reg_no: 'AP 09 AR 0092',
        num_plate: 'carplate.jpeg',
        num_plate_color: 'White',
        vehicle_img: 'cartraffic.jpg',
        vehicle_type: 'Car',
        color: 'Black',
        vehicle_make: 'Toyota',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'car.jpg'
    },
    // {
    //     reg_no: 'AP 11 L 007',
    //     num_plate: 'policevechicleplate.jpg',
    //     num_plate_color: 'Light White',
    //     vehicle_img: 'Policetraffic.jpg',
    //     vehicle_type: 'JEEP',
    //     color: 'White',
    //     vehicle_make: 'Mahendra',
    //     helmet: 'N/A',
    //     triples: 'N/A',
    //     full_image: 'policevechicle.jpg'
    // },
    {
        reg_no: 'AP 28 BB 3190',
        num_plate: 'scootyplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'scootytraffic.jpg',
        vehicle_type: 'Motor Bike',
        color: 'RED',
        vehicle_make: 'Vespa',
        helmet: 'Yes',
        triples: 'Yes',
        full_image: 'scooter.jpg'
    },
    {
        reg_no: 'AP 15 AE 2012',
        num_plate: 'truckplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'trucktraffic.jpg',
        vehicle_type: 'Truck',
        color: 'Brown',
        vehicle_make: 'TATA EICHER',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'truck.jpg'
    },
    {
        reg_no: 'AP 29 BP 585',
        num_plate: 'jeepplate.jpg',
        num_plate_color: 'White',
        vehicle_img: 'jeeptraffic.jpg',
        vehicle_type: 'Jeep',
        color: 'Brown',
        vehicle_make: 'Mahendhra',
        helmet: 'N/A',
        triples: 'N/A',
        full_image: 'jeeptraffic.jpg'
    },
    ]

}