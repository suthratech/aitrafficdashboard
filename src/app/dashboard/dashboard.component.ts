import { Component, OnInit } from '@angular/core';
import { PoliceService } from '../police.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  junctionsList: any = [
    {name:'Junction 1', value:'j1'},
    {name:'Junction 2', value:'j2'},
    {name:'Junction 3', value:'j3'},
    {name:'Junction 4', value:'j4'},      
    {name:'Junction 5', value:'j5'},
    {name:'Junction 6', value:'j6'},
    {name:'Junction 7', value:'j7'},
    {name:'Junction 8', value:'j8'},
    {name:'Junction 9', value:'j9'}
  ];
  fromdate = new Date(new Date().setHours(0,0,0,0));
  todate = new Date(new Date().setHours(0,0,0,0));
  selectedJunction:any;
  dropdown:any=[
    {name:'MTD', value:'mtd'},
    {name:'YTD', value:'ytd'}
  ];
  selecteddrType:any;
  dynamicCards:any=[];
  constructor(private service:PoliceService) { }

  ngOnInit() {
    this.generateDynamicCardsData();
  }



  generateDynamicCardsData(){
    this.service.getVehiclesData().subscribe(res=>{
      let resp:any = res;
      let data= resp.response;
      this.dynamicCards = [
        { vehicle_name: 'Cars', count: data.cars, icon: 'drive_eta', cssClass: 'first_circle' },
        { vehicle_name: 'Bus', count: data.bus, icon: 'directions_bus', cssClass: 'second_circle' },
        { vehicle_name: 'Motor Bikes', count: data.motor_bikes, icon: 'motorcycle', cssClass: 'third_circle' },
        { vehicle_name: 'Emergency', count: data.emergency, icon: 'airport_shuttle', cssClass: 'fourth_circle' },
        { vehicle_name: 'Auto', count: data.auto, icon: 'supervisor_account', cssClass: 'fourth_circle' },
        { vehicle_name: 'Riders', count: data.riders, icon: 'directions_bike', cssClass: 'third_circle' },
        { vehicle_name: 'Trucks', count: data.trucks, icon: 'local_shipping', cssClass: 'second_circle' },
        { vehicle_name: 'Pedestrians', count: data.pedestrians, icon: 'directions_walk', cssClass: 'first_circle' },
      ];
  });
  }



}
